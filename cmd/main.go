package main

import (
	"author/internal/database"
	"author/internal/http"
	"fmt"
	"log"
)

func main() {
	db := database.NewDb("pgx", fmt.Sprintf("postgres://postgres:postgres@localhost:5432/author?sslmode=disable"))

	err := db.Connect()
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		err = db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	server := http.NewServer(db)

	server.Run()
}
