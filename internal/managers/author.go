package managers

import (
	"author/internal/database"
	"author/pkg/models"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
)

type Authorer interface {
	AddAuthor(author *models.Author) error
	UpdateAuthor(author *models.Author) error
	Authors(page, limit int) ([]*models.Author, error)
	AuthorBooks(id int) (*models.AuthorBooks, error)
	DeleteAuthor(id int) error
}

type Author struct {
	authorRepository database.Authorer
}

func NewAuthor(authorRepository database.Authorer) Authorer {
	return &Author{
		authorRepository: authorRepository,
	}
}

func (a *Author) AddAuthor(author *models.Author) error {
	return a.authorRepository.AddAuthor(author)
}

func (a *Author) UpdateAuthor(author *models.Author) error {
	return a.authorRepository.UpdateAuthor(author)
}

func (a *Author) Authors(page, limit int) ([]*models.Author, error) {
	return a.authorRepository.Authors(page, limit)
}

func (a *Author) AuthorBooks(id int) (*models.AuthorBooks, error) {
	authorBooks, err := a.authorRepository.AuthorBooks(id)
	if err != nil {
		return nil, err
	}

	for i := range authorBooks {
		book, err := request(authorBooks[i].BookID)
		if err != nil {
			continue
		}

		authorBooks[0].Books = append(authorBooks[i].Books, book)
	}

	return authorBooks[0], nil
}

func (a *Author) DeleteAuthor(id int) error {
	return a.authorRepository.DeleteAuthor(id)
}

func request(id int) (*models.Book, error) {
	url := fmt.Sprintf("http://localhost:8080/v1/gateway/book?id=%d", id)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, errors.New("error")
	}

	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	book := new(models.Book)

	err = json.Unmarshal(body, book)
	if err != nil {
		return nil, err
	}

	return book, nil
}
