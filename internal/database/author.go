package database

import (
	"author/pkg/models"
	"context"
	"database/sql"
	"github.com/jmoiron/sqlx"
)

type Authorer interface {
	AddAuthor(author *models.Author) error
	UpdateAuthor(author *models.Author) error
	Authors(page, limit int) ([]*models.Author, error)
	AuthorBooks(id int) ([]*models.AuthorBooks, error)
	DeleteAuthor(id int) error
}

type Author struct {
	db *sqlx.DB
}

func NewAuthor(db *sqlx.DB) Authorer {
	return &Author{
		db: db,
	}
}

func (a *Author) AddAuthor(author *models.Author) error {
	query := `INSERT INTO author(first_name, last_name, pseudonym) VALUES (:first_name, :last_name, :pseudonym)`

	args := map[string]interface{}{
		"first_name": author.FirstName,
		"last_name":  author.LastName,
		"pseudonym":  author.Pseudonym,
	}

	_, err := a.db.NamedExec(query, args)
	if err != nil {
		return err
	}

	return nil
}

func (a *Author) UpdateAuthor(author *models.Author) error {
	query := `UPDATE author SET first_name=:first_name, last_name=:last_name, pseudonym=:pseudonym where id=:id`

	args := map[string]interface{}{
		"id":         author.ID,
		"first_name": author.FirstName,
		"last_name":  author.LastName,
		"pseudonym":  author.Pseudonym,
	}

	_, err := a.db.NamedExec(query, args)
	if err != nil {
		return err
	}

	return nil
}

func (a *Author) Authors(page, limit int) ([]*models.Author, error) {
	authors := make([]*models.Author, 0)

	row, err := a.db.Queryx(`SELECT * FROM author ORDER BY ID DESC LIMIT $1 OFFSET $2`, limit, page)
	if err != nil {
		return nil, err
	}

	for row.Next() {
		var author models.Author

		err = row.StructScan(&author)
		if err != nil {
			return nil, err
		}

		authors = append(authors, &author)
	}

	if len(authors) == 0 {
		return nil, sql.ErrNoRows
	}

	return authors, nil
}

func (a *Author) AuthorBooks(id int) ([]*models.AuthorBooks, error) {
	authorBooks := make([]*models.AuthorBooks, 0)

	query := `SELECT a.id,a.first_name, a.last_name, a.pseudonym, ab.book_id
			  FROM author a INNER JOIN author_book ab on a.id = ab.author_id
              WHERE ab.author_id = $1`

	row, err := a.db.Queryx(query, id)
	if err != nil {
		return nil, err
	}

	for row.Next() {
		authorBook := new(models.AuthorBooks)
		authorBook.Author = new(models.Author)

		err = row.Scan(&authorBook.ID, &authorBook.FirstName, &authorBook.LastName, &authorBook.Pseudonym, &authorBook.BookID)
		if err != nil {
			return nil, err
		}

		authorBooks = append(authorBooks, authorBook)
	}

	if len(authorBooks) == 0 {
		return nil, sql.ErrNoRows
	}

	return authorBooks, nil
}

func (a Author) DeleteAuthor(id int) error {
	tx, err := a.db.BeginTxx(context.Background(), nil)
	if err != nil {
		return err
	}

	tx.MustExec("DELETE FROM author_book WHERE author_id = $1", id)
	tx.MustExec("DELETE FROM author WHERE id = $1", id)

	err = tx.Commit()
	if err != nil {
		e := tx.Rollback()
		if err != nil {
			return e
		}

		return err
	}

	return nil
}
