package database

import (
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
)

type DataStore interface {
	Base
	AuthorRepository() Authorer
}

type Base interface {
	Connect() error
	Close() error
}

type DB struct {
	driver, dbStr    string
	db               *sqlx.DB
	authorRepository Authorer
}

func NewDb(driver, dbStr string) DataStore {
	return &DB{
		driver: driver,
		dbStr:  dbStr,
	}
}

func (d *DB) AuthorRepository() Authorer {
	if d.authorRepository == nil {
		d.authorRepository = NewAuthor(d.db)
	}

	return d.authorRepository
}

func (d *DB) Connect() error {
	db, err := sqlx.Connect(d.driver, d.dbStr)
	if err != nil {
		return err
	}

	err = db.Ping()
	if err != nil {
		return err
	}

	d.db = db

	return nil
}

func (d *DB) Close() error {
	return d.db.Close()
}
