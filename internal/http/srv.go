package http

import (
	"author/internal/database"
	"author/internal/managers"
	healthCheck "github.com/RaMin0/gin-health-check"
	"github.com/gin-gonic/gin"
	"log"
)

type server struct {
	router *gin.Engine
	db     database.DataStore
}

func NewServer(db database.DataStore) *server {
	return &server{
		router: gin.New(),
		db:     db,
	}
}

func (srv *server) setupRouter() {
	srv.router.Use(gin.Recovery())
	srv.router.Use(healthCheck.Default())

	v1 := srv.router.Group("/v1")

	authorManager := managers.NewAuthor(srv.db.AuthorRepository())
	NewAuthor(authorManager).Init(v1)
}

func (srv *server) Run() {
	srv.setupRouter()

	if err := srv.router.Run(":8081"); err != nil {
		log.Fatal(err)
	}
}
