package http

import (
	"author/internal/managers"
	"author/pkg/models"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type author struct {
	authorManager managers.Authorer
}

func NewAuthor(authorManager managers.Authorer) *author {
	return &author{
		authorManager: authorManager,
	}
}

func (a *author) Init(router *gin.RouterGroup) {
	route := router.Group("/author")

	route.POST("", a.createAuthor)
	route.PUT("/:id", a.updateAuthor)
	route.GET("/:id", a.author)
	route.GET("/authors", a.authors)
	route.DELETE("/:id", a.deleteAuthor)
}

func (a *author) createAuthor(ctx *gin.Context) {
	requestBody := new(models.Author)

	if err := ctx.BindJSON(requestBody); err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	err := a.authorManager.AddAuthor(requestBody)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	ctx.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}

func (a *author) updateAuthor(ctx *gin.Context) {
	idParam := ctx.Param("id")

	id, err := strconv.Atoi(idParam)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	requestBody := new(models.Author)

	if err = ctx.BindJSON(requestBody); err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	requestBody.ID = id

	err = a.authorManager.UpdateAuthor(requestBody)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	ctx.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}

func (a *author) author(ctx *gin.Context) {
	idParam := ctx.Param("id")

	id, err := strconv.Atoi(idParam)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	response, err := a.authorManager.AuthorBooks(id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusOK, response)
}

func (a *author) authors(ctx *gin.Context) {
	page := ctx.Query("page")
	limit := ctx.Query("limit")

	response, err := a.authorManager.Authors(models.HandlePagination(limit, page))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	if len(response) == 0 {
		ctx.JSON(http.StatusNotFound, models.CustomResponse{
			Code:    http.StatusNotFound,
			Message: http.StatusText(http.StatusNotFound),
		})

		return
	}

	ctx.JSON(http.StatusOK, response)
}

func (a *author) deleteAuthor(ctx *gin.Context) {
	idParam := ctx.Param("id")

	id, err := strconv.Atoi(idParam)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	err = a.authorManager.DeleteAuthor(id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}
