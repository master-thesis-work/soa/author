package models

type Author struct {
	ID        int    `json:"id" db:"id"`
	FirstName string `json:"first_name" db:"first_name"`
	LastName  string `json:"last_name" db:"last_name"`
	Pseudonym string `json:"pseudonym" db:"pseudonym"`
}

type AuthorBooks struct {
	*Author
	BookID int     `json:"-" db:"book_id"`
	Books  []*Book `json:"books"`
}
