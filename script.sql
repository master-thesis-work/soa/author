create table author
(
    id         serial
        constraint author_pk
            primary key,
    first_name varchar,
    last_name  varchar,
    pseudonym  varchar
);

alter table author
    owner to postgres;

create unique index author_id_uindex
    on author (id);

create table author_book
(
    id        serial
        constraint author_book_pk
            primary key,
    author_id integer
        references author,
    book_id   integer
);

alter table author_book
    owner to postgres;

create unique index author_book_id_uindex
    on author_book (id);

